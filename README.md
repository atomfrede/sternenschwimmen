[![codecov](https://codecov.io/gl/atomfrede/sternenschwimmen/branch/master/graph/badge.svg)](https://codecov.io/gl/atomfrede/sternenschwimmen)

# Schwwimmen untern Sternenhimmel

## Purpose

Eine einfache Anwendung um das alljährliche Schwimmen untern Sternenhimmel zu unterstützen:

* Teilnehmer verwalten
* Urkunden drucken
* Uebersicht der Veranstaltung
* [Video Preview](https://gitlab.com/atomfrede/sternenschwimmen/uploads/6b00ea03bbe10edc6bfe3b028753df3f/Screencast_19.06.2016_21_34_21.webm.webm)

## JHipster

This application was generated using JHipster, you can find documentation and help at [https://jhipster.github.io](https://jhipster.github.io).

[JHipster]: https://jhipster.github.io/
[Node.js]: https://nodejs.org/
[Bower]: http://bower.io/
[Gulp]: http://gulpjs.com/
[BrowserSync]: http://www.browsersync.io/
[Karma]: http://karma-runner.github.io/
[Jasmine]: http://jasmine.github.io/2.0/introduction.html
[Protractor]: https://angular.github.io/protractor/
