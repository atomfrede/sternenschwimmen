package com.gitlab.atomfrede.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "sternenschwimmen")
public class SternenschwimmenProperties {

    public Integer laneLength = 50;
    public Double pricePerLane = 0.20;

    public Integer getLaneLength() {
        return laneLength;
    }

    public void setLaneLength(Integer laneLength) {
        this.laneLength = laneLength;
    }

    public Double getPricePerLane() {
        return pricePerLane;
    }

    public void setPricePerLane(Double pricePerLane) {
        this.pricePerLane = pricePerLane;
    }
}
