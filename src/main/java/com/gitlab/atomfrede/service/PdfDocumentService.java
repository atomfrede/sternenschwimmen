package com.gitlab.atomfrede.service;

import com.gitlab.atomfrede.domain.Participant;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.font.PDFont;
import org.apache.pdfbox.pdmodel.font.PDType1Font;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;

@Service
public class PdfDocumentService {

    public File createCertificate(Participant participant) throws IOException {

        try (PDDocument doc = new PDDocument()) {

            File file = File.createTempFile(participant.getFirstname() + "_" + participant.getLastname(), ".pdf");

            PDPage page = new PDPage();
            doc.addPage(page);

            PDFont font = PDType1Font.HELVETICA_BOLD;

            PDPageContentStream contents = new PDPageContentStream(doc, page);
            contents.beginText();
            contents.setFont(font, 12);
            contents.newLineAtOffset(100, 700);
            contents.showText(participant.getFirstname());
            contents.endText();
            contents.close();

            doc.save(file);

            return file;

        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }
}
