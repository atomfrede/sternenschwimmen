/**
 * Data Transfer Objects used by Spring MVC REST controllers.
 */
package com.gitlab.atomfrede.web.rest.dto;
