package com.gitlab.atomfrede.web.rest.dashboard;

import java.math.BigDecimal;

public class MoneyEarned {

    public BigDecimal amount;

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }
}
