package com.gitlab.atomfrede.web.rest.dashboard;

import com.codahale.metrics.annotation.Timed;
import com.gitlab.atomfrede.config.SternenschwimmenProperties;
import com.gitlab.atomfrede.domain.Participant;
import com.gitlab.atomfrede.repository.ParticipantRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;
import java.math.BigDecimal;
import java.net.URISyntaxException;
import java.util.List;

@RestController
@RequestMapping("/api/dashboard")
public class DashboardResource {

    private final Logger log = LoggerFactory.getLogger(DashboardResource.class);

    @Inject
    private SternenschwimmenProperties sternenschwimmenProperties;

    @Inject
    private ParticipantRepository participantRepository;

    @RequestMapping(value = "/participants",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<ParticipantCount> getParticipantCount() throws URISyntaxException {
        log.debug("REST request to get Participant Count");

        long count = participantRepository.count();

        ParticipantCount pc = new ParticipantCount();
        pc.count = count;

        return ResponseEntity.ok(pc);

    }

    @RequestMapping(value = "/lanes",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<LaneCount> getLaneCount() throws URISyntaxException {
        log.debug("REST request to get Lane Count");

        List<Participant> allWithAtLeastOneLane = participantRepository.findAllByLaneCountGreaterThanEqual(1);
        LaneCount lc = new LaneCount();

        int lanes = 0;
        for (Participant participant : allWithAtLeastOneLane) {
            lanes = lanes + participant.getLaneCount();
        }
        lc.count = lanes;

        return ResponseEntity.ok(lc);

    }

    @RequestMapping(value = "/meter",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<MeterCount> getMetercount() throws URISyntaxException {
        log.debug("REST request to get Meter Count");

        List<Participant> allWithAtLeastOneLane = participantRepository.findAllByLaneCountGreaterThanEqual(1);

        int lanes = 0;
        for (Participant participant : allWithAtLeastOneLane) {
            lanes = lanes + participant.getLaneCount();
        }
        MeterCount mc = new MeterCount();
        mc.count = lanes * sternenschwimmenProperties.laneLength;

        return ResponseEntity.ok(mc);

    }

    @RequestMapping(value = "/money",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<MoneyEarned> getEarnedMoney() throws URISyntaxException {
        log.debug("REST request to get Money Count");

        List<Participant> allWithAtLeastOneLane = participantRepository.findAllByLaneCountGreaterThanEqual(1);

        int lanes = 0;
        for (Participant participant : allWithAtLeastOneLane) {
            lanes = lanes + participant.getLaneCount();
        }
        MoneyEarned mc = new MoneyEarned();
        BigDecimal amount = new BigDecimal("" + sternenschwimmenProperties.getPricePerLane()).multiply(new BigDecimal("" + lanes));
        mc.amount = amount;

        return ResponseEntity.ok(mc);

    }

}
