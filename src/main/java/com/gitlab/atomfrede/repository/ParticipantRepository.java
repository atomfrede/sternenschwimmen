package com.gitlab.atomfrede.repository;

import com.gitlab.atomfrede.domain.Participant;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Spring Data JPA repository for the Participant entity.
 */
@SuppressWarnings("unused")
public interface ParticipantRepository extends JpaRepository<Participant,Long> {

    List<Participant> findAllByLaneCountGreaterThanEqual(Integer atLeast);
}
