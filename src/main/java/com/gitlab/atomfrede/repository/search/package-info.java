/**
 * Spring Data ElasticSearch repositories.
 */
package com.gitlab.atomfrede.repository.search;
