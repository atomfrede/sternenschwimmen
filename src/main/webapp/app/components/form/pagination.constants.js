(function() {
    'use strict';

    angular
        .module('sternenschwimmenApp')
        .constant('paginationConstants', {
            'itemsPerPage': 12
        });
})();
