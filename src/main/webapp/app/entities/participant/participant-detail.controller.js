(function() {
    'use strict';

    angular
        .module('sternenschwimmenApp')
        .controller('ParticipantDetailController', ParticipantDetailController);

    ParticipantDetailController.$inject = ['$scope', '$rootScope', '$timeout', '$stateParams', 'PreviousState', 'entity', 'Participant'];

    function ParticipantDetailController($scope, $rootScope, $timeout, $stateParams, PreviousState, entity, Participant) {
        var vm = this;

        vm.participant = entity;

        vm.previousState = PreviousState.Name;
        var unsubscribe = $rootScope.$on('sternenschwimmenApp:participantUpdate', function(event, result) {
            vm.participant = result;
        });

        vm.print = function() {
            $timeout(reloadParticipant, 1000);

        };

        function reloadParticipant() {
            Participant.get({id : vm.participant.id}).$promise.then(function(participant) {
                vm.participant = participant;
            });
        }
        $scope.$on('$destroy', unsubscribe);
    }
})();
