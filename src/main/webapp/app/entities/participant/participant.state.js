(function() {
    'use strict';

    angular
        .module('sternenschwimmenApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('participant', {
            parent: 'entity',
            url: '/participant?page&sort&search',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'sternenschwimmenApp.participant.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/participant/participants.html',
                    controller: 'ParticipantController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'id,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }],
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('participant');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('participant-detail', {
            parent: 'entity',
            url: '/participant/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'sternenschwimmenApp.participant.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/participant/participant-detail.html',
                    controller: 'ParticipantDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('participant');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'Participant', function($stateParams, Participant) {
                    return Participant.get({id : $stateParams.id}).$promise;
                }],
                PreviousState: [
                    "$state", "$location",
                    function ($state, $location) {
                        var currentStateData = {
                            Name: $state.current.name || 'participant',
                            Params: $state.params,
                            URL: $location.url()
                        };
                        return currentStateData;
                    }
                ]
            }
        })
            .state('participant-detail.edit', {
                parent: 'participant-detail',
                url: '/detail/edit',
                data: {
                    authorities: ['ROLE_USER']
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'app/entities/participant/participant-dialog.html',
                        controller: 'ParticipantDialogController',
                        controllerAs: 'vm',
                        backdrop: 'static',
                        size: 'lg',
                        resolve: {
                            entity: ['Participant', function(Participant) {
                                return Participant.get({id : $stateParams.id}).$promise;
                            }]
                        }
                    }).result.then(function() {
                        $state.go('^', {}, { reload: false });
                    }, function() {
                        $state.go('^');
                    });
                }]
            })
        .state('participant.new', {
            parent: 'participant',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/participant/participant-dialog.html',
                    controller: 'ParticipantDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                firstname: null,
                                lastname: null,
                                sponsor: null,
                                payed: null,
                                printed: null,
                                laneCount: null,
                                team: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('participant', null, { reload: true });
                }, function() {
                    $state.go('participant');
                });
            }]
        })
        .state('participant.edit', {
            // parent: 'participant',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/participant/participant-dialog.html',
                    controller: 'ParticipantDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Participant', function(Participant) {
                            return Participant.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('participant', null, { reload: true });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('participant.delete', {
            parent: 'participant',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/participant/participant-delete-dialog.html',
                    controller: 'ParticipantDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['Participant', function(Participant) {
                            return Participant.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('participant', null, { reload: true });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
