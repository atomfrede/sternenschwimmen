(function() {
    'use strict';

    angular
        .module('sternenschwimmenApp')
        .controller('HomeController', HomeController);

    HomeController.$inject = ['$scope', 'Principal', 'LoginService', '$state', 'Dashboard'];

    function HomeController ($scope, Principal, LoginService, $state, Dashboard) {
        var vm = this;

        vm.account = null;
        vm.isAuthenticated = null;
        vm.participantCount = null;
        vm.meterCount = null;
        vm.laneCount = null;
        vm.moneyAmount = null;
        vm.login = LoginService.open;
        vm.register = register;
        $scope.$on('authenticationSuccess', function() {
            getAccount();
            getDashboardData()
        });

        getAccount();

        if (vm.isAuthenticated) {
            getDashboardData();
        }


        function getAccount() {

            Principal.identity().then(function(account) {
                vm.account = account;
                vm.isAuthenticated = Principal.isAuthenticated;
                if (vm.isAuthenticated) {
                    getDashboardData();
                }
            });
        }
        function register () {
            $state.go('register');
        }

        function getDashboardData() {
            Dashboard.participants({}, function(participantCount) {
                vm.participantCount = participantCount.count;
            });

            Dashboard.participants({}, function(participantCount) {
                vm.participantCount = participantCount.count;
            });

            Dashboard.lanes({}, function(laneCount) {
                vm.laneCount = laneCount.count;
            });

            Dashboard.meter({}, function(meterCount) {
                vm.meterCount = meterCount.count;
            });

            Dashboard.money({}, function(moneyEarned) {
                vm.moneyAmount = moneyEarned.amount;
            })
        }

    }
})();
