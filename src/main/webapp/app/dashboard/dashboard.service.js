(function() {
    'use strict';
    angular
        .module('sternenschwimmenApp')
        .factory('Dashboard', Dashboard);

    Dashboard.$inject = ['$resource'];

    function Dashboard ($resource) {
        var resourceUrl =  'api/dashboard/:part';

        return $resource(resourceUrl, {part: "@part"}, {
            'participants': { method: 'GET', params: {part: 'participants'}},
            'lanes': {method: 'GET', params: {part: 'lanes'}},
            'meter': {method: 'GET', params: {part: 'meter'}},
            'money': {method: 'GET', params: {part: 'money'}}

        });
    }
})();
