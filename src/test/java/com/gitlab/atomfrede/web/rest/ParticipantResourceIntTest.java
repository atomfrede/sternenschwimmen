package com.gitlab.atomfrede.web.rest;

import com.gitlab.atomfrede.SternenschwimmenApp;
import com.gitlab.atomfrede.domain.Participant;
import com.gitlab.atomfrede.repository.ParticipantRepository;
import com.gitlab.atomfrede.repository.search.ParticipantSearchRepository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


/**
 * Test class for the ParticipantResource REST controller.
 *
 * @see ParticipantResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = SternenschwimmenApp.class)
@WebAppConfiguration
@IntegrationTest
public class ParticipantResourceIntTest {

    private static final String DEFAULT_FIRSTNAME = "AAAAA";
    private static final String UPDATED_FIRSTNAME = "BBBBB";
    private static final String DEFAULT_LASTNAME = "AAAAA";
    private static final String UPDATED_LASTNAME = "BBBBB";
    private static final String DEFAULT_SPONSOR = "AAAAA";
    private static final String UPDATED_SPONSOR = "BBBBB";

    private static final Boolean DEFAULT_PAYED = false;
    private static final Boolean UPDATED_PAYED = true;

    private static final Boolean DEFAULT_PRINTED = false;
    private static final Boolean UPDATED_PRINTED = true;

    private static final Integer DEFAULT_LANE_COUNT = 1;
    private static final Integer UPDATED_LANE_COUNT = 2;
    private static final String DEFAULT_TEAM = "AAAAA";
    private static final String UPDATED_TEAM = "BBBBB";

    @Inject
    private ParticipantRepository participantRepository;

    @Inject
    private ParticipantSearchRepository participantSearchRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    private MockMvc restParticipantMockMvc;

    private Participant participant;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        ParticipantResource participantResource = new ParticipantResource();
        ReflectionTestUtils.setField(participantResource, "participantSearchRepository", participantSearchRepository);
        ReflectionTestUtils.setField(participantResource, "participantRepository", participantRepository);
        this.restParticipantMockMvc = MockMvcBuilders.standaloneSetup(participantResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        participantSearchRepository.deleteAll();
        participant = new Participant();
        participant.setFirstname(DEFAULT_FIRSTNAME);
        participant.setLastname(DEFAULT_LASTNAME);
        participant.setSponsor(DEFAULT_SPONSOR);
        participant.setPayed(DEFAULT_PAYED);
        participant.setPrinted(DEFAULT_PRINTED);
        participant.setLaneCount(DEFAULT_LANE_COUNT);
        participant.setTeam(DEFAULT_TEAM);
    }

    @Test
    @Transactional
    public void createParticipant() throws Exception {
        int databaseSizeBeforeCreate = participantRepository.findAll().size();

        // Create the Participant

        restParticipantMockMvc.perform(post("/api/participants")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(participant)))
                .andExpect(status().isCreated());

        // Validate the Participant in the database
        List<Participant> participants = participantRepository.findAll();
        assertThat(participants).hasSize(databaseSizeBeforeCreate + 1);
        Participant testParticipant = participants.get(participants.size() - 1);
        assertThat(testParticipant.getFirstname()).isEqualTo(DEFAULT_FIRSTNAME);
        assertThat(testParticipant.getLastname()).isEqualTo(DEFAULT_LASTNAME);
        assertThat(testParticipant.getSponsor()).isEqualTo(DEFAULT_SPONSOR);
        assertThat(testParticipant.isPayed()).isEqualTo(DEFAULT_PAYED);
        assertThat(testParticipant.isPrinted()).isEqualTo(DEFAULT_PRINTED);
        assertThat(testParticipant.getLaneCount()).isEqualTo(DEFAULT_LANE_COUNT);
        assertThat(testParticipant.getTeam()).isEqualTo(DEFAULT_TEAM);

        // Validate the Participant in ElasticSearch
        Participant participantEs = participantSearchRepository.findOne(testParticipant.getId());
        assertThat(participantEs).isEqualToComparingFieldByField(testParticipant);
    }

    @Test
    @Transactional
    public void checkFirstnameIsRequired() throws Exception {
        int databaseSizeBeforeTest = participantRepository.findAll().size();
        // set the field null
        participant.setFirstname(null);

        // Create the Participant, which fails.

        restParticipantMockMvc.perform(post("/api/participants")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(participant)))
                .andExpect(status().isBadRequest());

        List<Participant> participants = participantRepository.findAll();
        assertThat(participants).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkLastnameIsRequired() throws Exception {
        int databaseSizeBeforeTest = participantRepository.findAll().size();
        // set the field null
        participant.setLastname(null);

        // Create the Participant, which fails.

        restParticipantMockMvc.perform(post("/api/participants")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(participant)))
                .andExpect(status().isBadRequest());

        List<Participant> participants = participantRepository.findAll();
        assertThat(participants).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllParticipants() throws Exception {
        // Initialize the database
        participantRepository.saveAndFlush(participant);

        // Get all the participants
        restParticipantMockMvc.perform(get("/api/participants?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(participant.getId().intValue())))
                .andExpect(jsonPath("$.[*].firstname").value(hasItem(DEFAULT_FIRSTNAME.toString())))
                .andExpect(jsonPath("$.[*].lastname").value(hasItem(DEFAULT_LASTNAME.toString())))
                .andExpect(jsonPath("$.[*].sponsor").value(hasItem(DEFAULT_SPONSOR.toString())))
                .andExpect(jsonPath("$.[*].payed").value(hasItem(DEFAULT_PAYED.booleanValue())))
                .andExpect(jsonPath("$.[*].printed").value(hasItem(DEFAULT_PRINTED.booleanValue())))
                .andExpect(jsonPath("$.[*].laneCount").value(hasItem(DEFAULT_LANE_COUNT)))
                .andExpect(jsonPath("$.[*].team").value(hasItem(DEFAULT_TEAM.toString())));
    }

    @Test
    @Transactional
    public void getParticipant() throws Exception {
        // Initialize the database
        participantRepository.saveAndFlush(participant);

        // Get the participant
        restParticipantMockMvc.perform(get("/api/participants/{id}", participant.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(participant.getId().intValue()))
            .andExpect(jsonPath("$.firstname").value(DEFAULT_FIRSTNAME.toString()))
            .andExpect(jsonPath("$.lastname").value(DEFAULT_LASTNAME.toString()))
            .andExpect(jsonPath("$.sponsor").value(DEFAULT_SPONSOR.toString()))
            .andExpect(jsonPath("$.payed").value(DEFAULT_PAYED.booleanValue()))
            .andExpect(jsonPath("$.printed").value(DEFAULT_PRINTED.booleanValue()))
            .andExpect(jsonPath("$.laneCount").value(DEFAULT_LANE_COUNT))
            .andExpect(jsonPath("$.team").value(DEFAULT_TEAM.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingParticipant() throws Exception {
        // Get the participant
        restParticipantMockMvc.perform(get("/api/participants/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateParticipant() throws Exception {
        // Initialize the database
        participantRepository.saveAndFlush(participant);
        participantSearchRepository.save(participant);
        int databaseSizeBeforeUpdate = participantRepository.findAll().size();

        // Update the participant
        Participant updatedParticipant = new Participant();
        updatedParticipant.setId(participant.getId());
        updatedParticipant.setFirstname(UPDATED_FIRSTNAME);
        updatedParticipant.setLastname(UPDATED_LASTNAME);
        updatedParticipant.setSponsor(UPDATED_SPONSOR);
        updatedParticipant.setPayed(UPDATED_PAYED);
        updatedParticipant.setPrinted(UPDATED_PRINTED);
        updatedParticipant.setLaneCount(UPDATED_LANE_COUNT);
        updatedParticipant.setTeam(UPDATED_TEAM);

        restParticipantMockMvc.perform(put("/api/participants")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(updatedParticipant)))
                .andExpect(status().isOk());

        // Validate the Participant in the database
        List<Participant> participants = participantRepository.findAll();
        assertThat(participants).hasSize(databaseSizeBeforeUpdate);
        Participant testParticipant = participants.get(participants.size() - 1);
        assertThat(testParticipant.getFirstname()).isEqualTo(UPDATED_FIRSTNAME);
        assertThat(testParticipant.getLastname()).isEqualTo(UPDATED_LASTNAME);
        assertThat(testParticipant.getSponsor()).isEqualTo(UPDATED_SPONSOR);
        assertThat(testParticipant.isPayed()).isEqualTo(UPDATED_PAYED);
        assertThat(testParticipant.isPrinted()).isEqualTo(UPDATED_PRINTED);
        assertThat(testParticipant.getLaneCount()).isEqualTo(UPDATED_LANE_COUNT);
        assertThat(testParticipant.getTeam()).isEqualTo(UPDATED_TEAM);

        // Validate the Participant in ElasticSearch
        Participant participantEs = participantSearchRepository.findOne(testParticipant.getId());
        assertThat(participantEs).isEqualToComparingFieldByField(testParticipant);
    }

    @Test
    @Transactional
    public void deleteParticipant() throws Exception {
        // Initialize the database
        participantRepository.saveAndFlush(participant);
        participantSearchRepository.save(participant);
        int databaseSizeBeforeDelete = participantRepository.findAll().size();

        // Get the participant
        restParticipantMockMvc.perform(delete("/api/participants/{id}", participant.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate ElasticSearch is empty
        boolean participantExistsInEs = participantSearchRepository.exists(participant.getId());
        assertThat(participantExistsInEs).isFalse();

        // Validate the database is empty
        List<Participant> participants = participantRepository.findAll();
        assertThat(participants).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchParticipant() throws Exception {
        // Initialize the database
        participantRepository.saveAndFlush(participant);
        participantSearchRepository.save(participant);

        // Search the participant
        restParticipantMockMvc.perform(get("/api/_search/participants?query=id:" + participant.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.[*].id").value(hasItem(participant.getId().intValue())))
            .andExpect(jsonPath("$.[*].firstname").value(hasItem(DEFAULT_FIRSTNAME.toString())))
            .andExpect(jsonPath("$.[*].lastname").value(hasItem(DEFAULT_LASTNAME.toString())))
            .andExpect(jsonPath("$.[*].sponsor").value(hasItem(DEFAULT_SPONSOR.toString())))
            .andExpect(jsonPath("$.[*].payed").value(hasItem(DEFAULT_PAYED.booleanValue())))
            .andExpect(jsonPath("$.[*].printed").value(hasItem(DEFAULT_PRINTED.booleanValue())))
            .andExpect(jsonPath("$.[*].laneCount").value(hasItem(DEFAULT_LANE_COUNT)))
            .andExpect(jsonPath("$.[*].team").value(hasItem(DEFAULT_TEAM.toString())));
    }
}
